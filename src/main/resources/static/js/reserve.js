$(function(){
	$(".icnOpen").on("click",function(){
		var date=$(this).data("date");
		var time=$(this).data("time");
		var strDate=date.substring(0,4)+"年"+date.substring(5,7)+"月"+date.substring(8,10)+"日";
		var strTime=time.substring(0,5);
		var userId=$(this).data("userid");
		var coupleId=$(this).data("coupleid");
		var coupleName=$(this).data("couplename");
		var date2="<input type='hidden' name='date' value='"+date+"'>";
		var time2="<input type='hidden' name='time' value='"+time+"'>";
		$("#selectedDate").html(strDate);
		$("#selectedTime").html(strTime);
		$("#theDate").html(date2);
		$("#theTime").html(time2);
		$("#reserveConfirm").dialog();
	});
	$("#reserveComplete").on("click",function(){
		var date=$("input[name='date']").val();
		var time=$("input[name='time']").val();
		var userId=$("input[name='userId']").val();
		var coupleId=$("input[name='coupleId']").val();
		$("#reserveConfirm").dialog("close");
		$.ajax({
			url: "http://localhost:8090/dance/reserve/result",
			type: "POST",
			dataType: "text",
			data: "userId="+userId+"&coupleId="+coupleId+"&date="+date+"&time="+time,
		}).done(function(data){
			$("#reserveCreated").dialog();
		}).fail(function(){
			$("#reserveFault").dialog();
		});
		return false;
	});
	$(".reserveCancel").on("click",function(){
		var date=$(this).data("date");
		var time=$(this).data("time");
		var strDate=date.substring(0,4)+"年"+date.substring(5,7)+"月"+date.substring(8,10)+"日";
		var strTime=time.substring(0,5);
		var id=$(this).data("id");
		var couple=$(this).data("couple");
		var hiddenId="<input type='hidden' name='id' value='"+id+"'>";
		$("#cancelDate").html(strDate);
		$("#cancelTime").html(strTime);
		$("#cancelCouple").html(couple);
		$("#hiddenId").html(hiddenId);
		$("#cancelConfirm").dialog();

		//リロード防止
		return false;
	});
	$("#cancelSubmit").on("click",function(){
		var id=$("input[name='id']").val();
		$("#cancelConfirm").dialog("close");
		$.ajax({
			url: "http://localhost:8090/dance/reserve/cancel/result",
			type: "POST",
			dataType: "text",
			data: "id="+id,
		}).done(function(data){
			$("#cancelComplete").dialog();
		}).fail(function(){
			$("#cancelFault").dialog();
		});
		return false;
	});
});