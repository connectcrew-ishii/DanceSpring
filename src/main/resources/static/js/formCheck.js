$(function(){
	//ニックネーム・カナ
	$("#nickname").blur(function(){
		if($(this).val() == ""){
			$("#nicknameDialog").dialog()
		}
	});
	$("#reNickname").blur(function(){
		if($(this).val() == ""){
			$("#nicknameDialog").dialog()
		}else if($("#nickname").val() != $(this).val()){
			$("#nicknameConfirm").dialog()
		}
	});

	$("#kana").blur(function(){
		if($(this).val() == ""){
			$("#kanaDialog").dialog()
		}
	});
	$("#kana2").blur(function(){
		if($(this).val() == ""){
			$("#kanaDialog").dialog()
		}
	});
	$("#reKana").blur(function(){
		if($(this).val() == ""){
			$("#kanaDialog").dialog()
		}else if($("#kana").val() != $(this).val()){
			$("#kanaConfirm").dialog()
		}
	});
	$("#reKana2").blur(function(){
		if($(this).val() == ""){
			$("#kanaDialog").dialog()
		}else if($("#kana2").val() != $(this).val()){
			$("#kanaConfirm").dialog()
		}
	});

	//名前
	$("#name").blur(function(){
		if($(this).val() == ""){
			$("#nameDialog").dialog()
		}
	});
	$("#reName").blur(function(){
		if($(this).val() == ""){
			$("#nameDialog").dialog()
		}else if($("#name").val() != $(this).val()){
			$("#nameConfirm").dialog()
		}
	});
	$("#name2").blur(function(){
		if($(this).val() == ""){
			$("#nameDialog").dialog()
		}
	});
	$("#reName2").blur(function(){
		if($(this).val() == ""){
			$("#nameDialog").dialog()
		}else if($("#name2").val() != $(this).val()){
			$("#nameConfirm").dialog()
		}
	});
	//パスワード
	$("#passFail").on("click",function(){
		$("#pass").val("");
		$("#rePass").val("");
		$(this).dialog("close");
		return false;
	});
	$("#pass").blur(function(){
		if($(this).val() == ""){
			$("#passDialog").dialog()
		}
	});
	$("#rePass").blur(function(){
		if($(this).val() == ""){
			$("#passDialog").dialog()
		}else if($("#pass").val() != $(this).val()){
			$("#passConfirm").dialog()
		}
	});
	//メールアドレス
	$("#mail").blur(function(){
		if($(this).val() == ""){
			$("#mailDialog").dialog()
		}else if(!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
			$("#mailChara").dialog()
		}
	});
	$("#reMail").blur(function(){
		if($(this).val() == ""){
			$("#mailDialog").dialog()
		}else if($("#mail").val() != $(this).val()){
			$("#mailConfirm").dialog()
		}else if(!$(this).val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
			$("#mailChara").dialog()
		}
	});

	//メールアドレスが登録済みかどうか
	$("#email").blur(function(){
		var mail=$(this).val();
		if(mail==""){
			$("#mailDialog").dialog();
			return false;
		}
		$.ajax({
			url: "http://localhost:8090/dance/mailcheck",
			type: "POST",
			dataType: "text",
			data: "mail="+mail,
		}).done(function(data){
			if(data=="true"){
				$("#button").attr("disabled",false);
			}else{
				$("#mailDeplicated").dialog();
			}
		}).fail(function(data){
			alert("通信失敗");
		});
		return false;
	});

	//住所
	$("#ynum").blur(function(){
		if($(this).val() == ""){
			$("#ynumDialog").dialog()
		}else if(!$(this).val().match(/^[0-9]+$/)){
			$("#ynumConfirm").dialog()
		}
	});
	$("#reYnum").blur(function(){
		if($(this).val() == ""){
			$("#ynumDialog").dialog()
		}else if(!$(this).val().match(/^[0-9]+$/)){
			$("#ynumChara").dialog()
		}else if($("#ynum").val() != $(this).val()){
			$("#ynumConfirm").dialog()
		}
	});
	$("#address2").blur(function(){
		if($(this).val() == ""){
			$("#addressDialog").dialog()
		}
	});
	$("#reAddress2").blur(function(){
		if($(this).val() == ""){
			$("#addressDialog").dialog()
		}else if($("input[name='address2']").val() != $(this).val()){
			$("#addressConfirm").dialog()
		}
	});
	//電話番号
	$("#phone").blur(function(){
		if($(this).val() == ""){
			$("#phoneDialog").dialog()
		}else if(!$(this).val().match(/^[0-9\-]+$/)){
			$("#phoneChara").dialog()
		}
	});
	$("#rePhone").blur(function(){
		if($(this).val() == ""){
			$("#phoneDialog").dialog()
		}else if($("#phone").val() != $(this).val()){
			$("#phoneConfirm").dialog()
		}else if(!$(this).val().match(/^[0-9\-]+$/)){
			$(this).val("")
			$("#phoneChara").dialog()
		}
	});
	$("#text").blur(function(){
		if($(this).val() == ""){
			$("#textDialog").dialog()
		}
	});
	$("#title").blur(function(){
		if($(this).val() == ""){
			$("#titleDialog").dialog()
		}
	});

	//退会処理
	$("#deleteAcount").on("click",function(){
		var id="<input type='hidden' id='deleteId' value='"+$("#acountId").val()+"'>"
		$("#insertId").html(id);
		$("#acountConfirm").dialog();
		return false;
	});
	$("#delete").on("click",function(){
		var id=$("#deleteId").val();

		$.ajax({
			url: "http://localhost:8090/dance/deleteacount",
			type: "POST",
			dataType: "text",
			data: "id="+id,
		}).done(function(data){
			$("#deleteComplete").dialog();
		}).fail(function(){
			$("#deleteFault").dialog();
		});
		return false;
	});
	/*
	<div id="nicknameDialog" title="入力ミスがあります" style="display:none;"><p>ニックネームを入力してください</p></div>
	<div id="nameDialog" title="入力ミスがあります" style="display:none;"><p>名前を入力してください</p></div>
	<div id="kanaDialog" title="入力ミスがあります" style="display:none;"><p>名前(カナ)を入力してください</p></div>
	<div id="passDialog" title="入力ミスがあります" style="display:none;"><p>パスワードを入力してください</p></div>
	<div id="mailDialog" title="入力ミスがあります" style="display:none;"><p>メールアドレスを入力してください</p></div>
	<div id="ynumDialog" title="入力ミスがあります" style="display:none;"><p>郵便番号を入力してください</p></div>
	<div id="addressDialog" title="入力ミスがあります" style="display:none;"><p>丁目・番地を入力してください</p></div>
	<div id="phoneDialog" title="入力ミスがあります" style="display:none;"><p>電話番号を入力してください</p></div>
	<div id="titleDialog" title="入力ミスがあります" style="display:none;"><p>タイトルを入力してください</p></div>
	<div id="textDialog" title="入力ミスがあります" style="display:none;"><p>内容を入力してください</p></div>

	<div id="nicknameConfirm" title="入力ミスがあります" style="display:none;"><p>ニックネームが一致しません</p></div>
	<div id="kanaConfirm" title="入力ミスがあります" style="display:none;"><p>名前(カナ)が一致しません</p></div>
	<div id="passConfirm" title="入力ミスがあります" style="display:none;"><p>パスワードが一致しません</p></div>
	<div id="ynumChara" title="入力ミスがあります" style="display:none;"><p>郵便番号は半角数字で入力してください</p></div>
	<div id="mailConfirm" title="入力が正しくありません" style="display:none;"><p>メールアドレスが一致しません</p></div>
	<div id="mailChara" title="入力が正しくありません" style="display:none;"><p>不正な文字が使用されています</p></div>
	<div id="phoneConfirm" title="入力が正しくありません" style="display:none;"><p>電話番号が一致しません</p></div>
	<div id="phoneChara" title="入力が正しくありません" style="display:none;"><p>不正な文字が使用されています</p></div>
	*/
});