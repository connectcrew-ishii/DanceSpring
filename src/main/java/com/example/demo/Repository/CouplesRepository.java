package com.example.demo.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.Entity.Couples;
@Repository
@Transactional
public interface CouplesRepository extends JpaRepository<Couples, Integer>{
	public Optional<Couples> findById(Integer id);
	public void deleteById(Integer id);

	@Modifying
	@Query(value = "UPDATE Couples c SET c.intro = :intro WHERE c.id = :id", nativeQuery = true)
	public void update(@Param("intro")String intro, @Param("id")int id);

	@Modifying
	@Query(value = "UPDATE Couples c SET c.del_flag = :flag WHERE c.id = :id", nativeQuery = true)
	public void deleteCouple(@Param("flag")int flag, @Param("id")int id);
}
