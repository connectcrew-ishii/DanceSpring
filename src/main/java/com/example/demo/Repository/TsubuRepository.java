package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Entity.Tsubu;

public interface TsubuRepository extends JpaRepository<Tsubu ,Integer>{
	public List<Tsubu> findAllByOrderByIdDesc();
}
