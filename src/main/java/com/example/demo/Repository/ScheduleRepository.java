package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.Entity.Schedule;
@Repository
@Transactional
public interface ScheduleRepository extends JpaRepository<Schedule, Integer>{
	/*@Query("SELECT sch FROM Schedule sch WHERE coupleID = :coupleID ORDER BY date")
	*public List<Schedule> reserveList(@Param("coupleID")String coupleID);
	*
	*@Query(value="SELECT DISTINCT schedule.coupleID, teacher.couple, schedule.date, schedule.time FROM "
	*		+ "Schedule schedule INNER JOIN Teacher teacher ON schedule.coupleID = teacher.coupleID WHERE schedule.name = :name ORDER BY date ",nativeQuery=true)
	*public List<Schedule> reservedAll(@Param("name")String name);
	*/

	//public List<Schedule> findByCoupleIdOrderByDate(@Param("coupleId")int coupleId);

	public List<Schedule> findByUserIdOrderByDate(int id);

	@Modifying
	@Query(value = "UPDATE schedule s SET s.del_flag = :flag WHERE s.id = :id", nativeQuery = true)
	public void update(@Param("flag")int flag, @Param("id")int id);
}
