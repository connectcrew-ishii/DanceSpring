package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.Magazine;
@Repository
public interface MagazineRepository extends JpaRepository<Magazine, Integer>{

}
