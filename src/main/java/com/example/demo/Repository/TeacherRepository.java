package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.Entity.Teacher;
@Repository
@Transactional
public interface TeacherRepository extends JpaRepository<Teacher, Integer>{
	//where idにできる
	@Modifying
	@Query("UPDATE Teacher t SET t.name = :name , t.img = :img , t.intro = :intro WHERE t.coupleId = :coupleId AND t.name = :name")
	public void updateTeacher(@Param("coupleId")int coupleId, @Param("name")String name,
			@Param("img")String img, @Param("intro")String intro);

	public List<Teacher> findByCoupleId(int coupleId);

	@Modifying
	@Query(value = "UPDATE teacher t SET t.del_flag = :flag WHERE t.id = :id", nativeQuery = true)
	public void update(@Param("flag")int flag, @Param("id")int id);
}
