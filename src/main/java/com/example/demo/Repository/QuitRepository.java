package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.Entity.Quit;

@Transactional
@Repository
public interface QuitRepository extends JpaRepository<Quit, Integer>{
	public List<Quit> findAllByOrderByDate();
}
