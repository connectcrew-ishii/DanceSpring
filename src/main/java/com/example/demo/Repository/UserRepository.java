package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.Entity.User;
@Transactional
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
					//<引,数>は<Entityクラス名,IDタイプ
	/*
	 * SQL文はここにかける
	 * @Query("select u from User u where u.email = :email")
	 * User findByCustomerEmail(@Param("email")String email);
	 *
	 * >
	 */

	/*@Query("INSERT INTO sample (Id,Pass,Mail,Ynum,Address,Phone,Maga) VALUES(:Id,:pass,:mail,:ynum,:address,:phone,:maga)")
	User Insert(@Param("name") Integer Id,@Param("pass") String pass
			,@Param("mail") String mail,@Param("ynum") String ynum,@Param("address") String address
			,@Param("phone") String phone,@Param("maga") int maga);*/
	public List<User> findByMail(String mail);

	public List<User> findByNicknameLike(String nickname);
	public List<User> findByNameLike(String name);
	public List<User> findByKanaLike(String kana);
	public List<User> findByPassLike(String pass);
	public List<User> findByMailLike(String mail);
	public List<User> findByYnumLike(String ynum);
	public List<User> findByAddressLike(String address);
	public List<User> findByPhoneLike(String phone);
	public List<User> findByMagaLike(int maga);
	public List<User> findByFlagLike(int flag);

	@Modifying
	@Query(value = "UPDATE User u SET u.del_flag = :flag WHERE u.id = :id", nativeQuery = true)
	public void deleteUser(@Param("flag")int flag, @Param("id")int id);


}
