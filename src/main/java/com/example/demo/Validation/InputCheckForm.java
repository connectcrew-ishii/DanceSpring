package com.example.demo.Validation;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InputCheckForm implements Serializable{
	private static final long serialVersionUID = 1L;

    @NotNull
    private String input;
    private String reInput;

}
