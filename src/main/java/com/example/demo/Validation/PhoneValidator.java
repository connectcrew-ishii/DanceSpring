package com.example.demo.Validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, String> {
	@Override
	public void initialize(Phone phone) {
	}
	@Override
	public boolean isValid(String input,ConstraintValidatorContext cxt) {
		if(input == null) {
			return false;
		}
		if(input.matches("^[0-9]{3}-[0-9]{4}-[0-9]{4}$")) {
			return true;
		}
		if(input.matches("^[0-9]{2}-[0-9]{4}-[0-9]{4}$")) {
			return true;
		}
		if(input.matches("^[0-9]{3}-[0-9]{3}-[0-9]{4}$")) {
			return true;
		}
		return input.matches("^[0-9]{4}-[0-9]{2}-[0-9]{4}$");
	}
}
