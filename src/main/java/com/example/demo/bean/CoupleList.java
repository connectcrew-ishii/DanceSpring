package com.example.demo.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.Entity.Couple;
import com.example.demo.Service.CouplesService;

@Component
public class CoupleList {
	// org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'coupleList'
	private List<Couple> coupleList;
	
	@Autowired
	private CouplesService couplesService;

	
	public CoupleList() throws NullPointerException{
		this.coupleList = this.couplesService.getCoupleList();
	}
	public List<Couple> getCoupleList(){
		return coupleList;
	}
	public void setCoupleList(List<Couple> list) {
		this.coupleList = list;
	}

	//singletonを上書き更新できないから、list add
	public void addCoupleList(Couple couple) {
		this.coupleList.add(couple);
	}
}

