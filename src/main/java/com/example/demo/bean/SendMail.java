package com.example.demo.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.example.demo.Entity.Magazine;
import com.example.demo.Entity.User;

@Component
public class SendMail {
	@Autowired
	private MailSender sender;
	public static final String LINE_SEPARATOR = System.getProperty("line.separator");
	private final String FROM = "sampleishii@gmail.com";

	public String getFrom() {
		return FROM;
	}

	//新規会員登録用のメソッド
	public void newUserSendMail(String mail, String registerTransition) {
		SimpleMailMessage msg = new SimpleMailMessage();
		String title = "新規会員登録のご案内";
		String text = "Riccardo & Yuliaダンススクールからのメールです。" + LINE_SEPARATOR +
					"以下のリンクから会員登録を行ってください。" + LINE_SEPARATOR + System.getenv("HEROKU_APP_ROUTE") + registerTransition +
					LINE_SEPARATOR + LINE_SEPARATOR + "心当たりのない方は削除してください。";
		msg.setFrom(FROM);
		msg.setTo(mail);
		msg.setSubject(title);
		msg.setText(text);

		this.sender.send(msg);
	}

	//メルマガ配信
	public void sendMagazine(Magazine m, List<User> userList) {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setSubject(m.getTitle());
		msg.setText(m.getText());
		msg.setFrom(FROM);
		for(User u:userList) {
			msg.setBcc(u.getMail());
		}
		this.sender.send(msg);
	}
}
