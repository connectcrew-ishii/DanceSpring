package com.example.demo.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.Entity.Tsubu;
import com.example.demo.Service.TsubuService;

@Component
public class Tweet {
	private List<Tsubu> tweetList;

	@Autowired
	private TsubuService tsubuService;
	
	public Tweet() {
		this.tweetList = this.tsubuService.tweetList();
	}

	public List<Tsubu> tweetList(){
		return tweetList;
	}
	public void setTweetList(List<Tsubu> list) {
		this.tweetList = list;
	}
}
