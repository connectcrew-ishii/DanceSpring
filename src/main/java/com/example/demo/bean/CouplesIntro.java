package com.example.demo.bean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.Entity.Couples;
import com.example.demo.Service.CouplesService;

@Component
public class CouplesIntro {
	private List<Couples> couplesIntro;

	@Autowired
	private CouplesService couplesService;
	
	public CouplesIntro() {
		this.couplesIntro = this.couplesService.introList();
	}
	public List<Couples> getCouplesIntro(){
		return couplesIntro;
	}
	public void setCouplesIntro(List<Couples> list) {
		this.couplesIntro = list;
	}

	public void addCouplesIntro(Couples couples) {
		couplesIntro.add(couples);
	}
}
