package com.example.demo.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.demo.Exception.ArgumentViolationException;
import com.example.demo.Exception.DateParseException;
import com.example.demo.Exception.FormIllegalArgumentException;
import com.example.demo.Exception.NotFoundElementException;
import com.example.demo.Exception.NullRecordException;
import com.example.demo.Exception.SQLIllegalStateException;
import com.example.demo.Exception.SessionTimeOutException;
import com.example.demo.Exception.UserDeplicatedException;

import lombok.extern.slf4j.Slf4j;
@Controller
@Slf4j
public class ExceptionController {
	private static final String OTHER_ERROR = "otherError";

	//例外処理
		@ExceptionHandler(NullRecordException.class)
		public String NullPointerHandler(NullRecordException np, HttpServletRequest req) {
			log.error(np.toString());
			np.printStackTrace();
			req.setAttribute("status", np.getStatus());
			req.setAttribute("text", np.getText());
			return OTHER_ERROR;
		}

		@ExceptionHandler(NotFoundElementException.class)
		public String NotFoundElementHandler(NotFoundElementException np, HttpServletRequest req) {
			log.error(np.toString());
			np.printStackTrace();
			req.setAttribute("status", np.getStatus());
			req.setAttribute("text", np.getText());
			return OTHER_ERROR;
		}

		@ExceptionHandler(DateParseException.class)
		public String DateParseHandler(DateParseException dp, HttpServletRequest req) {
			log.error(dp.toString());
			dp.printStackTrace();
			req.setAttribute("status", dp.getStatus());
			req.setAttribute("text", dp.getText());
			return OTHER_ERROR;
		}

		@ExceptionHandler(UserDeplicatedException.class)
		public String UserDeplicatedHandler(UserDeplicatedException np, HttpServletRequest req) {
			req.setAttribute("status", np.getStatus());
			req.setAttribute("text", np.getText());
			log.error(np.getMessage());
			np.printStackTrace();
			return OTHER_ERROR;
		}

		@ExceptionHandler(FormIllegalArgumentException.class)
		public String FormIllegalArgumentHandler(FormIllegalArgumentException fa, HttpServletRequest req) {
			log.error(fa.getMessage());
			fa.printStackTrace();
			req.setAttribute("status", fa.getStatus());
			req.setAttribute("text", fa.getText());
			return OTHER_ERROR;
		}

		@ExceptionHandler(SQLIllegalStateException.class)
		public String SQLIllegalStateHandler(SQLIllegalStateException fa, HttpServletRequest req) {
			log.error(fa.getMessage());
			fa.printStackTrace();
			req.setAttribute("status", fa.getStatus());
			req.setAttribute("text", fa.getText());
			return OTHER_ERROR;
		}

		@ExceptionHandler(ArgumentViolationException.class)
		public String ArgumentViolationHandler(ArgumentViolationException fa, HttpServletRequest req) {
			log.error(fa.getMessage());
			fa.printStackTrace();
			req.setAttribute("status", fa.getStatus());
			req.setAttribute("text", fa.getText());
			return OTHER_ERROR;
		}

		@ExceptionHandler(SessionTimeOutException.class)
		public String SessionTimeOutHandler(SessionTimeOutException fa, HttpServletRequest req) {
			log.error(fa.getMessage());
			fa.printStackTrace();
			req.setAttribute("status", fa.getStatus());
			req.setAttribute("text", fa.getText());
			return OTHER_ERROR;
		}
}
