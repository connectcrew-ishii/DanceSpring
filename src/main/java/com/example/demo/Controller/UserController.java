package com.example.demo.Controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Entity.Contact;
import com.example.demo.Entity.Quit;
import com.example.demo.Entity.Tsubu;
import com.example.demo.Entity.User;
import com.example.demo.Exception.ArgumentViolationException;
import com.example.demo.Exception.FormIllegalArgumentException;
import com.example.demo.Exception.NullRecordException;
import com.example.demo.Exception.SessionTimeOutException;
import com.example.demo.Exception.UserDeplicatedException;
import com.example.demo.Service.ContactService;
import com.example.demo.Service.QuitService;
import com.example.demo.Service.TeacherService;
import com.example.demo.Service.TsubuService;
import com.example.demo.Service.UserService;
import com.example.demo.Validation.GroupOrder;
import com.example.demo.bean.CoupleList;
import com.example.demo.bean.CouplesIntro;
import com.example.demo.bean.SendMail;
import com.example.demo.bean.Tweet;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/")
public class UserController extends ExceptionController{
	private static final String VIEW = "index";
	private static final String VIEW2 = "newUserResult";
	private static final String ERROR = "error";
	private static final String CONFIG = "configResult";
	private static final String C_FAULT = "configFault";

	@Autowired
	UserService userService;
	@Autowired
	TeacherService teacherService;
	@Autowired
	ContactService contactService;
	@Autowired
	TsubuService tsubuService;
	@Autowired
	CoupleList coupleList;
	@Autowired
	Tweet tweet;
	@Autowired
	CouplesIntro couplesIntro;
	@Autowired
	SendMail sendMail;
	@Autowired
	QuitService quitService;

	/**
	 * 引数にBindingResult入れずに
	 * BindExceptionとMethodArgumentNotValidExceptionを個別にハンドリングする?
	 */
	
	/**
	 * トップページ
	 * @param request
	 * @param session
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpSession session, ModelAndView mv) {
		mv.addObject("tweetList", tweet.tweetList());
		mv.setViewName(VIEW);
		return mv;
	}
	
	/**
	 * トップページ
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping("top")
	public ModelAndView top(ModelAndView mv) {
		mv.addObject("tweetList", tweet.tweetList());
		mv.setViewName(VIEW);
		return mv;
	}
	
	/**
	 * ログイン後トップページ
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping("main")
	public ModelAndView main(ModelAndView mv) {
		mv.addObject("tweetList", tweet.tweetList());
		mv.setViewName("main");
		return mv;
	}
	
	/**
	 * メールアドレス入力フォーム
	 * @return String
	 */
	@RequestMapping(value = "newuser")
	public String newUser() {
		return "newUserSendMail";
	}
	
	/**
	 * 登録済みメールアドレスかどうか
	 * @param mail
	 * @return String
	 */
	@ResponseBody
	@RequestMapping(value = "mailcheck", method = RequestMethod.POST)
	public String mailChecker(@RequestParam("mail")String mail) {
		return userService.isMailDeplicated(mail) ? "true" : "false";
		
	}
	
	/**
	 * 会員登録メール送信
	 * @param mail
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "newusersendmail")
	public String newUserSendMail(@RequestParam("mail")String mail, HttpSession session) {
		sendMail.newUserSendMail(mail, "userregister");
		session.setAttribute("mailaddress", mail);
		return "newUserMailResult";
	}
	
	/**
	 * 新規登録フォーム
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userregister")
	public String userRegister(HttpSession session) throws SessionTimeOutException{
		if(session.getAttribute("mailaddress") == null) {
			throw new SessionTimeOutException();
		}
		return "newUser";
	}

	/**
	 * 会員登録結果
	 * @param user
	 * @param result
	 * @param mov
	 * @param rePass
	 * @param address2
	 * @param name2
	 * @param kana2
	 * @return ModelaNdView
	 */
	@RequestMapping(value = "newuser", method = RequestMethod.POST, params = "form")
	public ModelAndView userList(@ModelAttribute User user, BindingResult result, ModelAndView mov,
			@RequestParam("rePass")String rePass, @RequestParam("address2")String address2, @RequestParam("name2")String name2,@RequestParam("kana2")String kana2)
			throws UserDeplicatedException, FormIllegalArgumentException, ArgumentViolationException{
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(user.getPass().equals(rePass)) {
			String k = user.getKana() + kana2;
			String n = user.getName() + name2;
			String a = user.getAddress() + address2;
			user.setKana(k);
			user.setAddress(a);
			user.setName(n);
			try {
				userService.reRegister(user);
				userService.newUser(user);
			}catch(SQLIntegrityConstraintViolationException e) {
				throw new UserDeplicatedException(e);
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException(e);
			}

			mov.setViewName(VIEW2);
			return mov;
		}
		mov.setViewName("newUserFault");
		return mov;
	}
	
	/**
	 * ログインページ
	 * @return String
	 */
	@RequestMapping(value = "login")
	public String login() {
		return "login";
	}
	
	/**
	 * ログイン結果
	 * @param mail
	 * @param pass
	 * @param session
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "main", method = RequestMethod.POST)
	public ModelAndView Vmain(@Validated(GroupOrder.class)@RequestParam("mail") String mail,
			@Validated(GroupOrder.class)@RequestParam("pass")String pass, HttpSession session, ModelAndView mv) throws FormIllegalArgumentException{
		if(!(pass instanceof String) || !(mail instanceof String)) {
			log.error(this.toString() + "：ログインに不正な値が使用されました");
			throw new FormIllegalArgumentException();
		}
		if(userService.isLogin(mail, pass)) {
			User user = userService.setUser(mail);
			userService.setAliveSchedule(user);
			String y = user.getYnum().substring(0, 3) + "-" + user.getYnum().substring(3, 7);
			session.setAttribute("ynum", y);
			session.setAttribute("user", user);
			mv.addObject("tweetList", tweet.tweetList());
			mv.setViewName("main");
			return mv;
		}
		mv.setViewName("loginFault");
		return mv;
	}
	
	/**
	 * 先生紹介
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "teacher", params = "before")
	public ModelAndView teacherB(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("teacher");
			return mv;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * 先生紹介
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "teacher")
	public ModelAndView teacher(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("teacher");
			return mv;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * 先生紹介
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "afterteacher")
	public ModelAndView afterTeacher(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("teacherAL");
			return mv;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * 先生紹介
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "teacher", params = "after")
	public ModelAndView teacherA(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("teacherAL");
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
		return mv;
	}
	
	/**
	 * 種目紹介
	 * @return String
	 */
	@RequestMapping(value = "syumoku", params = "before")
	public String syumokuB() {
		return "syumoku";
	}
	
	/**
	 * 種目紹介
	 * @return String
	 */
	@RequestMapping(value = "syumoku")
	public String syumokuBe() {
		return "syumoku";
	}
	
	/**
	 * 種目紹介
	 * @return String
	 */
	@RequestMapping(value = "syumoku", params = "after")
	public String syumokuA() {
		return "syumokuAL";
	}
	
	/**
	 * 種目紹介
	 * @return String
	 */
	@RequestMapping(value = "aftersyumoku")
	public String syumokuAf() {
		return "syumokuAL";
	}
	
	/**
	 * ユーザー設定
	 * @return String
	 */
	@RequestMapping(value = "config")
	public String config() {
		return "config";
	}
	
	/**
	 * ニックネーム変更
	 * @return String
	 */
	@RequestMapping(value = "config", params = "nickname")
	public String configNickname() {
		return "configNickname";
	}
	
	/**
	 * ニックネーム変更結果
	 * @param user
	 * @param session
	 * @param reNickname
	 * @return String
	 */
	@RequestMapping(value = "config", params = "nickname", method = RequestMethod.POST)
	public String configNciknameResult(@ModelAttribute User user, BindingResult result, HttpSession session,
			@RequestParam("reNickname")String reNickname) throws FormIllegalArgumentException{
		User u = (User)session.getAttribute("user");
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(user.getNickname().equals(reNickname)) {
			u.setNickname(user.getNickname());
			userService.update(u);
			session.setAttribute("user", u);
			return CONFIG;
		}
		return C_FAULT;
	}
	
	/**
	 * 名前変更
	 * @return String
	 */
	@RequestMapping(value = "config", params = "name")
	public String configName() {
		return "configID";
	}
	
	/**
	 * 名前変更結果
	 * @param user
	 * @param result
	 * @param session
	 * @param reName
	 * @param name2
	 * @param reName2
	 * @param reKana
	 * @param kana2
	 * @param reKana2
	 * @return String
	 */
	@RequestMapping(value = "config", params = "name", method = RequestMethod.POST)
	public String configNameResult(@ModelAttribute User user, BindingResult result, HttpSession session, @RequestParam("reName")String reName
			,@RequestParam("name2")String name2, @RequestParam("reName2")String reName2, @RequestParam("reKana")String reKana,
			@RequestParam("kana2")String kana2, @RequestParam("reKana2")String reKana2)  throws FormIllegalArgumentException{
		User u = (User)session.getAttribute("user");
		String n = user.getName() + name2;
		String nn = reName + reName2;
		String k = user.getKana() + kana2;
		String kk = reKana + reKana2;
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(n.equals(nn) && k.equals(kk)) {
			u.setName(n);
			u.setKana(k);
			userService.update(u);
			session.setAttribute("user", u);
			return CONFIG;
		}
		return C_FAULT;
	}
	
	/**
	 * パスワード変更
	 * @return String
	 */
	//
	@RequestMapping(value = "config", params = "pass")
	public String configPass() {
		return "configPass";
	}
	
	/**
	 * パスワード変更結果
	 * @param user
	 * @param result
	 * @param session
	 * @param rePass
	 * @return String
	 */
	@RequestMapping(value = "config", params = "pass", method = RequestMethod.POST)
	public String configPassResult(@ModelAttribute User user, BindingResult result,
			HttpSession session, @RequestParam("rePass")String rePass)  throws FormIllegalArgumentException{
		User u = (User)session.getAttribute("user");
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(rePass.equals(user.getPass())) {
			u.setPass(user.getPass());
			userService.update(u);
			session.setAttribute("user", u);
			return CONFIG;
		}
		return C_FAULT;
	}
	
	/**
	 * メールアドレス変更
	 * @return String
	 */
	@RequestMapping(value = "config", params = "mail")
	public String configMail() {
		return "configMail";
	}
	
	/**
	 * メールアドレス変更結果
	 * @param user
	 * @param result
	 * @param session
	 * @param reMail
	 * @return String
	 */
	@RequestMapping(value = "config", params = "mail", method = RequestMethod.POST)
	public String configMailResult(@ModelAttribute User user, BindingResult result,
			HttpSession session, @RequestParam("reMail")String reMail)  throws FormIllegalArgumentException,UserDeplicatedException,ArgumentViolationException{
		User u = (User)session.getAttribute("user");
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(reMail.equals(user.getMail())) {
			try {
			userService.updateMail(u);
			u.setMail(user.getMail());
			session.setAttribute("user", u);
			return CONFIG;

			}catch(SQLIntegrityConstraintViolationException e) {
				throw new UserDeplicatedException(e);
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException (e);
			}
		}
		return C_FAULT;
	}
	
	/**
	 * 住所変更
	 * @return String
	 */
	@RequestMapping(value = "config", params = "address")
	public String configAddress() {
		return "configAddress";
	}
	
	/**
	 * 住所変更結果
	 * @param user
	 * @param result
	 * @param session
	 * @param reYnum
	 * @param address2
	 * @param reAddress
	 * @param reAddress2
	 * @return String
	 */
	@RequestMapping(value = "config", params = "address", method = RequestMethod.POST)
	public String configAddressResult(@ModelAttribute User user, BindingResult result, HttpSession session,
			@RequestParam("reYnum")String reYnum, @RequestParam("address2")String address2,
			@RequestParam("reAddress")String reAddress, @RequestParam("reAddress2")String reAddress2)  throws FormIllegalArgumentException,ArgumentViolationException{
		User u = (User)session.getAttribute("user");
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(reYnum.equals(user.getYnum()) && address2.equals(reAddress2)) {
			try {
				String address = reAddress + reAddress2;
				u.setYnum(user.getYnum());
				u.setAddress(address);
				userService.update(u);
				session.setAttribute("user", u);
				return CONFIG;
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException(e);
			}
		}
		return C_FAULT;
	}
	
	/**
	 * 電話番号変更
	 * @return String
	 */
	@RequestMapping(value = "config", params = "phone")
	public String configPhone() {
		return "configPhone";
	}
	
	/**
	 * 電話番号変更結果
	 * @param user
	 * @param result
	 * @param session
	 * @param rePhone
	 * @return String
	 */
	@RequestMapping(value = "config", params = "phone", method = RequestMethod.POST)
	public String configPhoneResult(@ModelAttribute User user, BindingResult result,
			HttpSession session, @RequestParam("rePhone")String rePhone)  throws FormIllegalArgumentException, ArgumentViolationException{
		User u = (User)session.getAttribute("user");
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(rePhone.equals(user.getPhone())) {
			try {
				u.setPhone(user.getPhone());
				userService.update(u);
				session.setAttribute("user", u);
				return CONFIG;
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException(e);
			}
		}
		return C_FAULT;
	}
	
	/**
	 * メルマガ設定
	 * @return String
	 */
	@RequestMapping(value = "config", params = "maga")
	public String configMaga() {
		return "configMaga";
	}
	
	/**
	 * メルマガ設定結果
	 * @param user
	 * @param result
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "config", params = "maga", method = RequestMethod.POST)
	public String configMagaResult(@ModelAttribute User user, BindingResult result, HttpSession session)  throws FormIllegalArgumentException{
		User u = (User)session.getAttribute("user");
		if(result.hasErrors()) {
			log.error(result.toString());
			throw new FormIllegalArgumentException();
		}
		if(user.getMaga() == 0 | user.getMaga() == 1) {
			u.setMaga(user.getMaga());
			userService.update(u);
			session.setAttribute("user", u);
			return CONFIG;
		}else {
			throw new FormIllegalArgumentException();
		}
	}
	
	/**
	 * 問い合わせ
	 * @return String
	 */
	@RequestMapping(value = "contact")
	public String contactForm() {
		return "contact";
	}
	
	/**
	 * 問い合わせ
	 * @return String
	 */
	@RequestMapping(value = "aftercontact")
	public String ContactForm() {
		return "contactAL";
	}
	
	/**
	 * 問い合わせ確認
	 * @param contact
	 * @param result
	 * @return String
	 */
	@RequestMapping(value = "contact", params = "confirm")
	public String contactConfirm(@ModelAttribute Contact contact, BindingResult result) {
		if(result.hasErrors()) {
			log.warn(result.toString());
			return "contactFault";
		}
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
		String date = sdf.format(d);
		String time = sdf2.format(d);
		contact.setDate(date);
		contact.setTime(time);
		contactService.create(contact);
		return "contactResult";
	}
	/**
	 * 問い合わせ確認
	 * @param contact
	 * @param result
	 * @return String
	 */
	@RequestMapping(value = "aftercontact", params = "confirm")
	public String contactConfirmAL(@ModelAttribute Contact contact, BindingResult result) {
		if(result.hasErrors()) {
			log.warn(result.toString());
			return "contactALFault";
		}
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
		String date = sdf.format(d);
		String time = sdf2.format(d);
		contact.setDate(date);
		contact.setTime(time);
		contactService.create(contact);
		return "contactALResult";
	}
	
	/**
	 * ログアウト
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "index";
	}
	
	/**
	 * ツイッター
	 * @param t
	 * @param result
	 * @param session
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "tweeter")
	public ModelAndView tweet(@ModelAttribute Tsubu t, BindingResult result, HttpSession session, ModelAndView mv) {
		if(result.hasErrors()) {
			log.error(result.toString());
			mv.setViewName(ERROR);
			return mv;
		}
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
		String date = sdf.format(d);
		String time = sdf2.format(d);
		User user = (User)session.getAttribute("user");
		t.setNickname(user.getNickname());
		t.setDate(date);
		t.setTime(time);
		tsubuService.create(t);
		tweet.setTweetList(tsubuService.tweetList());
		mv.addObject("tweetList", tweet.tweetList());
		mv.setViewName("main");
		return mv;
	}
	
	/**
	 * アクセス(マップ)
	 * @return String
	 */
	@RequestMapping(value = "access")
	public String access() {
		return "access";
	}
	
	/**
	 * アクセス(マップ)
	 * @return String
	 */
	@RequestMapping(value = "afteraccess")
	public String accessAL() {
		return "accessAL";
	}
	
	/**
	 * 料金案内
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "price")
	public ModelAndView price(ModelAndView mv) {
		mv.addObject("coupleList", coupleList.getCoupleList());
		mv.setViewName("price");
		return mv;
	}
	
	/**
	 * 料金案内
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "afterprice")
	public ModelAndView priceAL(ModelAndView mv) {
		mv.addObject("coupleList", coupleList.getCoupleList());
		mv.setViewName("priceAL");
		return mv;
	}
	
	/**
	 * 退会
	 * @return String
	 */
	@RequestMapping(value = "deleteacount")
	public String acount() {
		return "acountConfirm";
	}
	
	/**
	 * 退会確認
	 * @param id
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "deleteacount", method = RequestMethod.POST)
	public String acountDelete(@RequestParam("id")int id, HttpSession session){
		User user = (User)session.getAttribute("user");
		Quit quit = new Quit();
		int flag = 1;
		user.setFlag(flag);
		userService.update(user);
		quitService.setValue(quit,user);
		quitService.save(quit);
		return "acountConfirm";
	}
}

