package com.example.demo.Controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Entity.MyCalendar;
import com.example.demo.Entity.MyCalendarLogic;
import com.example.demo.Entity.Schedule;
import com.example.demo.Entity.User;
import com.example.demo.Exception.DateParseException;
import com.example.demo.Exception.FormIllegalArgumentException;
import com.example.demo.Exception.NotFoundElementException;
import com.example.demo.Exception.NullRecordException;
import com.example.demo.Exception.UserDeplicatedException;
import com.example.demo.Service.CouplesService;
import com.example.demo.Service.ScheduleService;
import com.example.demo.Service.UserService;
import com.example.demo.bean.CoupleList;
import com.example.demo.bean.CouplesIntro;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/reserve/")
public class ReserveController extends ExceptionController{
	@Autowired
	ScheduleService schService;
	@Autowired
	CoupleList coupleList;
	@Autowired
	CouplesIntro couplesIntro;
	@Autowired
	CouplesService couplesService;
	@Autowired
	UserService userService;
	
	/**
	 * 予約・一覧キャンセル
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "teacher")
	public ModelAndView reserveT(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("reserveTeacher");
			return mv;

		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}

	/**
	 * カレンダー
	 * @param coupleId
	 * @param session
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "calendar")
	public ModelAndView calendar(@RequestParam("whoReserve") int coupleId, HttpSession session, ModelAndView mv)
			throws FormIllegalArgumentException, NullRecordException, NotFoundElementException{
		try {
			couplesService.isValidCoupleId(coupleId);
			int week = 0;
			MyCalendarLogic cal = new MyCalendarLogic();
			MyCalendar c = new MyCalendar();
			String[] timeList = c.getTimeList();
			List<MyCalendar> daytimeList = cal.dayTimeList(week);
			List<Schedule> rList = couplesService.getCouple(coupleId).getScheduleList();
			int colspan = schService.colspan(week);
			User user = (User)session.getAttribute("user");

			mv.addObject("couple", couplesService.createCouple(coupleId));
			mv.addObject("couplesIntro", couplesService.getCouple(coupleId));

			//スケジュールかぶりを判別
			List<ArrayList<MyCalendar>> trueList = schService.trueList(week,rList,user);

			session.setAttribute("teacher", couplesService.getCouple(coupleId));
			session.setAttribute("rList", rList);
			session.setAttribute("colspan",	colspan);
			session.setAttribute("trueList", trueList);
			session.setAttribute("daytimeList", daytimeList);
			session.setAttribute("week", week);
			session.setAttribute("timeList", timeList);
			mv.setViewName("calendar");
			return mv;
		}catch(NoSuchElementException e) {
			throw new NotFoundElementException(e);
		}
	}
	
	/**
	 * weekの値でカレンダーずらす
	 * @param week
	 * @param session
	 * @param coupleId
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "calendars")
	public ModelAndView calendars(@RequestParam("week")int week, HttpSession session, @RequestParam("coupleId")int coupleId
			,ModelAndView mv) throws NullRecordException, FormIllegalArgumentException, NotFoundElementException {
		try {
			if(week < 0) {
				week = 0;
			}else if(week > 8) {
				week = 8;
			}
			couplesService.isValidCoupleId(coupleId);
			MyCalendarLogic cal = new MyCalendarLogic();
			List<MyCalendar> daytimeList = cal.dayTimeList(week);
			int colspan = schService.colspan(week);
			@SuppressWarnings("unchecked")
			List<Schedule> rList = (List<Schedule>)session.getAttribute("rList");
			List<ArrayList<MyCalendar>> trueList = schService.trueList(week, rList, (User)session.getAttribute("user"));

			mv.addObject("couple", couplesService.createCouple(coupleId));
			mv.addObject("couplesIntro", couplesService.getCouple(coupleId));

			session.setAttribute("trueList", trueList);
			session.setAttribute("colspan", colspan);
			session.setAttribute("daytimeList", daytimeList);
			session.setAttribute("week", week);
			mv.setViewName("calendar");
			return mv;

		}catch(NoSuchElementException e) {
			throw new NotFoundElementException(e);
		}
	}
	
	/**
	 * 予約する
	 * @param schedule
	 * @param result
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "result", method = RequestMethod.POST)
	public String Result(@ModelAttribute Schedule schedule, BindingResult result, HttpSession session) throws UserDeplicatedException{
		if(result.hasErrors()) {
			log.warn("Binding Error パラメータのバインドエラー");
			return "reserveFault";
		}
		try {
			schService.create(schedule);
			User user = userService.findById(schedule.getUserId());
			schedule.setCouples(couplesService.getCouple(schedule.getCoupleId()));
			schedule.setUser(userService.findById(schedule.getUserId()));
			session.setAttribute("user", user);
			return "reserveResult";
		}catch(SQLIntegrityConstraintViolationException e) {
			throw new UserDeplicatedException(e);
		}
	}
	
	/**
	 * 予約一覧
	 * @param session
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "all")
	public ModelAndView reservedAll(HttpSession session, ModelAndView mv) throws NullRecordException, DateParseException{
		try {
			User user = (User)session.getAttribute("user");
			mv.addObject("reservedAll", userService.getReserveList(user));
			mv.setViewName("reservedAll");
			return mv;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * キャンセル
	 * @param Id
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "cancel/result")
	public String CancelResult(@RequestParam("id")int id, HttpSession session){
		int flag = 1;
		User user = (User)session.getAttribute("user");
		schService.deleteSchedule(flag, id);
		user.setScheduleList(schService.findByUserId(user.getId()));
		//schService.deleteSchedule(user,id);
		session.setAttribute("user", user);
		return "reserveCancelResult";
	}
}
