package com.example.demo.Controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Entity.Couple;
import com.example.demo.Entity.Couples;
import com.example.demo.Entity.Magazine;
import com.example.demo.Entity.Quit;
import com.example.demo.Entity.Teacher;
import com.example.demo.Entity.User;
import com.example.demo.Exception.ArgumentViolationException;
import com.example.demo.Exception.FormIllegalArgumentException;
import com.example.demo.Exception.NotFoundElementException;
import com.example.demo.Exception.NullRecordException;
import com.example.demo.Exception.NumberFormedException;
import com.example.demo.Exception.UserDeplicatedException;
import com.example.demo.Service.CouplesService;
import com.example.demo.Service.MagazineService;
import com.example.demo.Service.QuitService;
import com.example.demo.Service.TeacherService;
import com.example.demo.Service.UserService;
import com.example.demo.bean.CoupleList;
import com.example.demo.bean.CouplesIntro;
import com.example.demo.bean.SendMail;
import com.example.demo.bean.Tweet;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/master/")
public class MasterController extends ExceptionController{
	private static final String C_FAULT = "masterUserConfigFault";

	@Autowired
	TeacherService teacherService;
	@Autowired
	UserService userService;
	@Autowired
	MagazineService magazineService;
	@Autowired
	Tweet tweet;
	@Autowired
	CoupleList coupleList;
	@Autowired
	CouplesIntro couplesIntro;
	@Autowired
	CouplesService couplesService;
	@Autowired
	QuitService quitService;
	@Autowired
	SendMail sendMail;
	
	/**
	 * ログイン
	 * @return String
	 */
	@RequestMapping("login")
	public String login() {
		return "masterLogin";
	}
	
	/**
	 * トップページ
	 * @param name
	 * @param pass
	 * @return String
	 */
	@RequestMapping("main")
	public String loginResult(@RequestParam("name")String name, @RequestParam("pass")String pass) throws FormIllegalArgumentException{
		if(!(name instanceof String) || !(pass instanceof String)) {
			log.error(this.toString() + "：ログインに不正な値が使用されました");
			throw new FormIllegalArgumentException();
		}
		//判別
		String i = System.getenv("MASTER_NAME");
		if(name.equals(i) && pass.equals(i)) {
			return "masterMain";
		}
		return "masterLoginFault";
	}
	
	/**
	 * トップページ
	 * @return String
	 */
	@RequestMapping("top")
	public String top() {
		return "masterMain";
	}
	
	/**
	 * 先生情報
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping("teacher")
	public ModelAndView teacher(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("masterTeacher");
			return mv;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * 先生情報(カップル)
	 * @param coupleId
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping("teacherconfig")
	public ModelAndView teacherConfig(@RequestParam("coupleId")int coupleId, ModelAndView mv)
			throws FormIllegalArgumentException, NotFoundElementException{
		try {
			couplesService.isValidCoupleId(coupleId);
			List<Couple> teacherConfig = teacherService.teacherConfig(coupleList.getCoupleList(), coupleId);
			mv.addObject("teacherConfig", teacherConfig);
			mv.addObject("couples", couplesService.getCouple(coupleId));
			mv.setViewName("masterTeacherConfig");
			return mv;
		}catch(NoSuchElementException e) {
			throw new NotFoundElementException(e);
		}
	}
	
	/**
	 * 先生情報変更
	 * @param couple
	 * @param coupleId
	 * @param imgL
	 * @param nameL
	 * @param introL
	 * @param imgP
	 * @param nameP
	 * @param introP
	 * @param leaderId
	 * @param partnerId
	 * @param couplesIntro
	 * @param ageL
	 * @param heightL
	 * @param weightL
	 * @param etcL
	 * @param ageP
	 * @param heightP
	 * @param weightP
	 * @param etcP
	 * @return String
	 */
	@RequestMapping(value = "teacherresult", params = "config")
	public String teacherResultC(@RequestParam("couple")String couple, @RequestParam("coupleId")int coupleId,
			@RequestParam("imgL")String imgL, @RequestParam("nameL")String nameL, @RequestParam("introL")String introL,
			@RequestParam("imgP")String imgP, @RequestParam("nameP")String nameP, @RequestParam("introP")String introP,
			@RequestParam("leaderId")int leaderId, @RequestParam("partnerId")int partnerId, @RequestParam("couplesIntro")String couplesIntro,
			@RequestParam("ageL")int ageL, @RequestParam("heightL")int heightL, @RequestParam("weightL")int weightL, @RequestParam("etcL")String etcL,
			@RequestParam("ageP")int ageP, @RequestParam("heightP")int heightP, @RequestParam("weightP")int weightP, @RequestParam("etcP")String etcP)
			throws FormIllegalArgumentException ,NotFoundElementException{
		if(leaderId != partnerId) {
			throw new FormIllegalArgumentException();
		}
		//リーダー・パートナー情報更新
		Teacher leader = new Teacher();
		leader.setId(leaderId);
		leader.setName(nameL);
		leader.setImg(imgL);
		leader.setIntro(introL);
		leader.setAge(ageL);
		leader.setHeight(heightL);
		leader.setWeight(weightL);
		leader.setEtc(etcL);

		Teacher partner = new Teacher();
		partner.setId(partnerId);
		partner.setName(nameP);
		partner.setImg(imgP);
		partner.setIntro(introP);
		partner.setAge(ageP);
		partner.setHeight(heightP);
		partner.setWeight(weightP);
		partner.setEtc(etcP);

		try {
			teacherService.save(leader);
			teacherService.save(partner);

			//カップル情報更新
			Couples c = new Couples();
			c.setId(coupleId);
			c.setName(couple);
			c.setIntro(couplesIntro);
			couplesService.update(c);
			return "masterTeacherResult";

		}catch(NoSuchElementException e){
			throw new NotFoundElementException(e);
		}
	}
	
	/**
	 * 先生追加
	 * @return String
	 */
	@RequestMapping("teacheradd")
	public String teacherAdd() {
		return "masterTeacherAdd";
	}
	
	/**
	 * 先生追加結果
	 * @param couple
	 * @param couplesIntro
	 * @param imgL
	 * @param nameL
	 * @param introL
	 * @param ageL
	 * @param heightL
	 * @param weightL
	 * @param countryL
	 * @param sexL
	 * @param personalityL
	 * @param eventL
	 * @param etcL
	 * @param imgP
	 * @param nameP
	 * @param introP
	 * @param ageP
	 * @param heightP
	 * @param weightP
	 * @param countryL
	 * @param sexL
	 * @param personalityL
	 * @param eventL
	 * @param etcP
	 * @return String
	 */
	@RequestMapping(value = "teacherresult", params = "add")
	public String teacherResultA(@RequestParam("couple")String couple, @RequestParam("couplesIntro")String couplesIntro,
			@RequestParam("imgL")String imgL, @RequestParam("nameL")String nameL, @RequestParam("introL")String introL, @RequestParam("ageL")int ageL,
			@RequestParam("heightL")int heightL ,@RequestParam("weightL")int weightL, @RequestParam("countryL")String countryL, @RequestParam("sexL")int sexL,
			@RequestParam("personalityL")String personalityL, @RequestParam("eventL")int eventL, @RequestParam("etcL")String etcL,
			@RequestParam("imgP")String imgP, @RequestParam("nameP")String nameP, @RequestParam("introP")String introP, @RequestParam("ageP")int ageP,
			@RequestParam("heightP")int heightP, @RequestParam("weightP")int weightP, @RequestParam("countryP")String countryP, @RequestParam("sexP")int sexP,
			@RequestParam("personalityP")String personalityP, @RequestParam("eventP")int eventP, @RequestParam("etcP")String etcP) throws NotFoundElementException{
		Couples c = new Couples();
		c.setName(couple);
		c.setIntro(couplesIntro);
		couplesService.create(c);
		this.couplesIntro.setCouplesIntro(couplesService.introList());
		int coupleId = couplesService.getNextCoupleNumber();

		Teacher leader = new Teacher();
		leader.setCoupleId(coupleId);
		leader.setName(nameL);
		leader.setImg(imgL);
		leader.setIntro(introL);
		leader.setAge(ageL);
		leader.setSex(sexL);
		leader.setHeight(heightL);
		leader.setWeight(weightL);
		leader.setCountry(countryL);
		leader.setPersonality(personalityL);
		leader.setEvent(eventL);
		leader.setEtc(etcL);

		Teacher partner = new Teacher();
		partner.setCoupleId(coupleId);
		partner.setName(nameP);
		partner.setImg(imgP);
		partner.setIntro(introP);
		partner.setAge(ageP);
		partner.setSex(sexP);
		partner.setHeight(heightP);
		partner.setWeight(weightP);
		partner.setCountry(countryP);
		partner.setPersonality(personalityP);
		partner.setEvent(eventP);
		partner.setEtc(etcP);

		try {
			teacherService.save(leader);
			teacherService.save(partner);
		}catch(NoSuchElementException e){
			throw new NotFoundElementException(e);
		}

		Couple cSet = new Couple();
		cSet.setCoupleId(coupleId);
		cSet.setName(couple);
		cSet.setLeader(leader);
		cSet.setPartner(partner);
		coupleList.addCoupleList(cSet);
		//↑追加削除を頻繁にするとcoupleIdがずれるかも。一番新しいid消すとずれるかと

		//coupleList.setCoupleList(couplesService.getCoupleList());

		return "masterTeacherResult";
	}
	
	/**
	 * 先生削除
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping("teacherdelete")
	public ModelAndView teacherDelete(ModelAndView mv) throws NullRecordException{
		try {
			mv.addObject("coupleList", coupleList.getCoupleList());
			mv.addObject("couplesIntro", couplesIntro.getCouplesIntro());
			mv.setViewName("masterTeacherDelete");
			return mv;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * 先生削除確認
	 * @param coupleId
	 * @param mv
	 * @param session
	 * @return ModelAndView
	 */
	@RequestMapping("teacherdeleteconfirm")
	public ModelAndView teacherDeleteConfirm(@RequestParam("coupleId")int coupleId, ModelAndView mv, HttpSession session) {
		List<Teacher> deleteTeacher = teacherService.teacherList(coupleId);
		mv.addObject("couple", couplesService.getCouple(coupleId));
		mv.addObject("deleteTeacher", deleteTeacher);
		mv.addObject("coupleId", coupleId);
		mv.setViewName("masterTeacherDeleteConfirm");
		session.setAttribute("deleteId", coupleId);
		return mv;
	}
	
	/**
	 * 先生削除結果
	 * @param coupleId
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "teacherresult", params = "delete")
	public String teacherResultD(@RequestParam("coupleId")int coupleId, HttpSession session) throws NullRecordException, FormIllegalArgumentException{
		int deleteId = (int)session.getAttribute("deleteId");
		if(deleteId != coupleId) {
			throw new FormIllegalArgumentException();
		}
		//couple teacherちゃんと消せてるか確認
		int flag = 1;
		teacherService.deleteTeacher(flag, coupleId);
		couplesService.deleteCouple(flag, coupleId);

		try{
			coupleList.setCoupleList(couplesService.getCoupleList());
			couplesIntro.setCouplesIntro(couplesService.introList());
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
		return "masterTeacherResult";
	}
	
	/**
	 * ユーザー一覧
	 * @param session
	 * @return String
	 */
	@RequestMapping("user")
	public String user(HttpSession session) {
		List<User> userList = userService.userList();
		session.setAttribute("userList", userList);
		return "masterUser";
	}
	
	/**
	 * ユーザー検索
	 * @param column
	 * @param text
	 * @return String
	 */
	@RequestMapping(value = "find", method = RequestMethod.POST)
	@ResponseBody
	public String find(@RequestParam("column")String column, @RequestParam("text")String text) throws NumberFormedException{
		List<User> userList;
		StringBuffer sb = new StringBuffer();
		int col;
		try {
			col = Integer.parseInt(column);
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}

		switch(col) {
		case 0:
			userList = userService.likeNickname("%" + text + "%");
			break;

		case 1:
			userList = userService.likeName("%" + text + "%");
			break;

		case 2:
			userList = userService.likePass("%" + text + "%");
			break;

		case 3:
			userList = userService.likeMail("%" + text + "%");
			break;

		case 4:
			userList = userService.likeYnum("%" + text + "%");
			break;

		case 5:
			userList = userService.likeAddress("%" + text + "%");
			break;

		case 6:
			userList = userService.likePhone("%" + text + "%");
			break;

		case 7:
			if(text == null || text == "") {
				String a = "<select name='text' id='text'><option value=1>受け取る</option><option value=0>受け取らない</option></select>" + "";
				sb.append(a);
				return sb.toString();
			}else {
				userList = userService.likeMaga(text);
				break;
			}

		case 8:
			userList = userService.likeKana("%" + text + "%");
			break;

		case 9:
			if(text == null || text == "") {
				String a = "<input type='hidden' value='quit' id='text'>";
				sb.append(a);
				return sb.toString();
			}else {
				String a = "<table border='1'><tr><th>ニックネーム</th><th>名前</th><th>パスワード</th><th>性別</th><th>メールアドレス</th>"
						+ "<th>住所</th><th>電話番号</th><th>etc</th></tr>";
				sb.append(a);
				for(Quit q:quitService.quitList()) {
					String sex = q.getSex() == 0 ? "男性" : "女性";
					String b = "<tr><td>" + q.getNickname() + "</td><td>" + q.getKana() + "<br>" + q.getName() + "</td><td>" + q.getPass() + "</td><td>" + sex + "</td><td>" +
							q.getMail() + "</td><td>" + q.getYnum() + "<br>" + q.getAddress() + "</td><td>" + q.getPhone() + "</td><td>" + q.getEtc() + "</td></tr>";
					sb.append(b);
				}
				sb.append("</table>");
				return sb.toString();
			}

		default:
			userList = new ArrayList<User>();
		}


		if(userList.isEmpty()) {
			return "<tr><td colspan='8'>該当なし</td></tr>";
		}
		for(User u:userList) {
			String a = "<tr><td>" + u.getName() + "</td><td>" + u.getPass() + "</td><td>" + u.getMail() + "</td><td>" + u.getYnum() + "</td><td>" + u.getAddress()+
					"</td><td>" + u.getPhone() + "</td><td>" + u.getMaga() + "</td></tr>";
			sb.append(a);
		}
        return sb.toString();
	}
	
	/**
	 * ユーザ情報
	 * @param session
	 * @param mail
	 * @return String
	 */
	@RequestMapping(value = "userconfig")
	public String userConfig(HttpSession session, @RequestParam("mail")String mail){
		User theUser = userService.theUser(mail);
		String y = theUser.getYnum().substring(0, 3) + "-" + theUser.getYnum().substring(3, 7);
		session.setAttribute("ynum", y);
		session.setAttribute("theUser", theUser);
		return "masterUserConfig";
	}
	
	/**
	 * ユーザ情報
	 * @return String
	 */
	@RequestMapping(value = "returnconfig")
	public String returnUserConfig() {
		return "masterUserConfig";
	}
	
	/**
	 * ニックネーム変更
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "nickname")
	public String userConfigNickname() {
		return "masterUserConfigNickname";
	}
	
	/**
	 * ニックネーム変更結果
	 * @param nickname
	 * @param reNickname
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "Nickname")
	public String userConfigNicknameResult(@RequestParam("nickname")String nickname,
			@RequestParam("reNickname")String reNickname, HttpSession session) {
		if(nickname.equals(reNickname)) {
			User user = (User)session.getAttribute("theUser");
			user.setNickname(nickname);
			userService.update(user);
			return "masterUserConfigResult";
		}
		return C_FAULT;
	}
	
	/**
	 * 名前変更
	 * @return Strirng
	 */
	@RequestMapping(value = "userconfig", params = "name")
	public String userConfigName() {
		return "masterUserConfigName";
	}
	
	/**
	 * 名前変更結果
	 * @param name
	 * @param reName
	 * @param session
	 * @param name2
	 * @param reName2
	 * @param kana
	 * @param kana2
	 * @param reKana
	 * @param reKana2
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Name")
	public String userConfigNameResult(@RequestParam("name")String name, @RequestParam("reName")String reName, HttpSession session
			,@RequestParam("name2")String name2, @RequestParam("reName2")String reName2, @RequestParam("kana")String kana,
			@RequestParam("kana")String kana2, @RequestParam("reKana")String reKana, @RequestParam("reKana2")String reKana2) {
		String n = name + reName;
		String nn = name2 + reName2;
		String k = kana + kana2;
		String kk = reKana + reKana2;
		if(n.equals(nn) && k.equals(kk)) {
			User user = (User)session.getAttribute("theUser");
			user.setName(n);
			user.setKana(k);
			userService.update(user);
			return "masterUserConfigResult";
		}
		return C_FAULT;
	}
	
	/**
	 * パスワード変更
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "pass")
	public String userConfigPass() {
		return "masterUserConfigPass";
	}
	
	/**
	 * パスワード変更結果
	 * @param pass
	 * @param rePass
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Pass")
	public String userConfigPassResult(@RequestParam("pass")String pass, @RequestParam("rePass")String rePass, HttpSession session) {
		if(pass.equals(rePass)) {
			User user = (User)session.getAttribute("theUser");
			user.setPass(pass);
			userService.update(user);
			return "masterUserConfigResult";
		}
		return C_FAULT;
	}
	
	/**
	 * メールアドレス変更
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "mail", method = RequestMethod.POST)
	public String userConfigMail() {
		return "masterUserConfigMail";
	}
	
	/**
	 * メールアドレス変更結果
	 * @param mail
	 * @param reMail
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Mail")
	public String userConfigMailResult(@RequestParam("mail")String mail,
			@RequestParam("reMail")String reMail, HttpSession session) throws UserDeplicatedException, ArgumentViolationException{
		if(mail.equals(reMail)) {
			User user = (User)session.getAttribute("theUser");
			user.setMail(mail);
			try {
				userService.updateMail(user);
			}catch(SQLIntegrityConstraintViolationException e) {
				throw new UserDeplicatedException(e);
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException(e);
			}
			return "masterUserConfigResult";
		}
		return C_FAULT;
	}
	
	/**
	 * 住所変更
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "address")
	public String userConfigAddress() {
		return "masterUserConfigAddress";
	}
	
	/**
	 * 住所変更結果
	 * @param ynum
	 * @param reYnum
	 * @param address
	 * @param address2
	 * @param reAddress2
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Address")
	public String userConfigAddressResult(@RequestParam("ynum")String ynum, @RequestParam("reYnum")String reYnum,
			@RequestParam("address")String address, @RequestParam("address2")String address2, @RequestParam("reAddress2")String reAddress2,HttpSession session)
			throws ArgumentViolationException{
		if(ynum.equals(reYnum) && address2.equals(reAddress2)) {
			try {
				User user = (User)session.getAttribute("theUser");
				user.setYnum(ynum);
				String a = address + address2;
				user.setAddress(a);
				userService.update(user);
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException(e);
			}
			return "masterUserConfigResult";
		}
		return C_FAULT;
	}
	
	/**
	 * 電話番号変更
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "phone")
	public String userConfigPhone() {
		return "masterUserConfigPhone";
	}
	
	/**
	 * 電話番号変更結果
	 * @param phone
	 * @param rePhone
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Phone")
	public String userConfigPhoneResult(@RequestParam("phone")String phone, @RequestParam("rePhone")String rePhone, HttpSession session)
			throws ArgumentViolationException{
		if(phone.equals(rePhone)) {
			try {
				User user = (User)session.getAttribute("theUser");
				user.setPhone(phone);
				userService.update(user);
				return "masterUserConfigResult";
			}catch(ConstraintViolationException e) {
				throw new ArgumentViolationException(e);
			}
		}
		return C_FAULT;
	}
	
	/**
	 * メルマガ設定変更
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "maga")
	public String userConfigMaga() {
		return "masterUserConfigMaga";
	}
	
	/**
	 * メルマガ設定変更結果
	 * @param maga
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Maga")
	public String userConfigMagaResult(@RequestParam("maga")String maga, HttpSession session) throws NumberFormedException, FormIllegalArgumentException{
		int m;
		try {
			m = Integer.parseInt(maga);
			if(m != 0 || m != 1) {
				throw new FormIllegalArgumentException();
			}
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}
		User user = (User)session.getAttribute("theUser");
		user.setMaga(m);
		userService.update(user);
		return "masterUserConfigResult";
	}
	
	/**
	 * アカウント登録状況
	 * @return String
	 */
	@RequestMapping(value = "userconfig", params = "acount")
	public String userConfigFlag() {
		return "masterUserConfigFlag";
	}
	
	/**
	 * 入会状態ユーザー一覧
	 * @param flag
	 * @param session
	 * @return String
	 */
	@RequestMapping(value = "userconfigresult", params = "Flag")
	public String userConfigFlagResult(@RequestParam("flag")String flag, HttpSession session) throws NumberFormedException, FormIllegalArgumentException{
		int f;
		try {
			f = Integer.parseInt(flag);
			if(f != 0 || f != 1) {
				throw new FormIllegalArgumentException();
			}
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}
		User user = (User)session.getAttribute("theUser");
		user.setFlag(f);
		userService.update(user);
		return "masterUserConfigResult";
	}
	
	/**
	 * メルマガ送信
	 * @return String
	 */
	@RequestMapping("mailmagazine")
	public String magazine() {
		return "masterMaga";
	}
	
	/**
	 * メルマガ送信確認 
	 * @param mv
	 * @param title
	 * @param text
	 * @return ModelAndView
	 */
	@RequestMapping("magazineconfirm")
	public ModelAndView magazineConfirm(ModelAndView mv, @RequestParam("title")String title, @RequestParam("text")String text) {
		mv.addObject("title", title);
		mv.addObject("text", text);
		mv.setViewName("masterMagaConfirm");
		return mv;
	}
	
	/**
	 * メルマガ送信結果
	 * @param m
	 * @param result
	 * @return String
	 */
	@RequestMapping("magazineresult")
	public String magazineResult(@ModelAttribute Magazine m, BindingResult result) throws FormIllegalArgumentException{
		if(result.hasErrors()) {
			throw new FormIllegalArgumentException();
		}
		//メール送信
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
		m.setDate(sdf.format(date));
		m.setTime(sdf2.format(date));
		sendMail.sendMagazine(m, userService.mailList());
		magazineService.create(m);
		return "masterMagaResult";
	}
	
	/**
	 * メルマガ配信ユーザー一覧
	 * @param mv
	 * @return ModelAndView
	 */
	@RequestMapping(value = "mailmagazineall")
	public ModelAndView all(ModelAndView mv) {
		List<Magazine> magaList = magazineService.magazineAll();
		mv.addObject("magaList", magaList);
		mv.setViewName("masterMagaAll");
		return mv;
	}
}
