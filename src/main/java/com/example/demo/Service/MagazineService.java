package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Magazine;
import com.example.demo.Repository.MagazineRepository;

@Service
public class MagazineService {
	@Autowired
	MagazineRepository repository;
	
	/**
	 * 配信したメルマガ内容を保存
	 * @param m
	 */
	public void create(Magazine m) {
		repository.save(m);
	}
	
	/**
	 * 過去配信分の一覧
	 * @return List<Magazine>
	 */
	public List<Magazine> magazineAll(){
		return repository.findAll();
	}
}
