package com.example.demo.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Couple;
import com.example.demo.Entity.Couples;
import com.example.demo.Exception.NullRecordException;
import com.example.demo.Repository.CouplesRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CouplesService {
	@Autowired
	CouplesRepository repository;
	
	/**
	 * カップル紹介一覧
	 * @return List<Couples>
	 */
	public List<Couples> introList(){
		List<Couples> list = new ArrayList<Couples>();
		for(Couples c:repository.findAll()) {
			if(c.getFlag() == 0) {
				list.add(c);
			}
		}
		return list;
	}
	
	/**
	 * カップル紹介更新
	 * @param c
	 * @return void
	 */
	public void update(Couples c) throws NoSuchElementException{
		repository.update(c.getIntro(), c.getId());
	}
	
	/**
	 * カップル削除
	 * @param flag
	 * @param coupleId
	 * @return void
	 */
	public void deleteCouple(int flag, int id) {
		repository.deleteCouple(flag, id);
	}
	
	/**
	 * カップル追加
	 * @param c
	 * @return void
	 */
	public void create(Couples c) {
		repository.save(c);
	}
	
	/**
	 * 指定したカップルの取得
	 * @param id
	 * @return Couples
	 */
	public Couples getCouple(int id) {
		Optional<Couples> c = repository.findById(id);
		return c.get();
	}
	
	/**
	 * カップル一覧
	 * @return List<Couple>
	 */
	public List<Couple> getCoupleList() throws NullPointerException{
		List<Couple> coupleList = new ArrayList<Couple>();
		for(Couples cc:repository.findAll()) {
			if(cc.getFlag() == 0) {
				Couple c = new Couple();
				c.setCoupleId(cc.getId());
				c.setName(cc.getName());
				c.setLeader(cc.getTeacherList().get(0));
				c.setPartner(cc.getTeacherList().get(1));
				coupleList.add(c);
			}
		}
		return coupleList;
	}
	
	/**
	 * カップル新規作成
	 * @param coupleId
	 * @return Couple
	 */
	public Couple createCouple(int coupleId) throws NullRecordException{
		try {
			Couples cc = getCouple(coupleId);
			Couple c = new Couple();
			c.setCoupleId(cc.getId());
			c.setName(cc.getName());
			c.setLeader(cc.getTeacherList().get(0));
			c.setPartner(cc.getTeacherList().get(1));
			return c;
		}catch(NullPointerException e) {
			throw new NullRecordException(e);
		}
	}
	
	/**
	 * 登録されている[CoupleId + 1]を返す
	 * @return int
	 */
	public int getNextCoupleNumber() {
		List<Couples> couplesList = repository.findAll();
		int number = couplesList.get(0).getId();
		for(int i = 0; i < couplesList.size() - 1; i++) {
			if(couplesList.get(i).getId() <= couplesList.get(i + 1).getId()) {
				number = couplesList.get(i + 1).getId();
			}
		}
		return number;
	}
	
	/**
	 * 登録されている最小の[CoupleId]を返す
	 * @return int
	 */
	public int getFirstNumber() {
		List<Couples> couplesList = repository.findAll();
		int number = couplesList.get(0).getId();
		for(int i = 0; i < couplesList.size() - 1; i++) {
			if(couplesList.get(i).getId() >= couplesList.get(i + 1).getId()) {
				number = couplesList.get(i + 1).getId();
			}
		}
		return number;
	}
	
	/**
	 * 指定された[CoupleId]のカップル情報を返す
	 * @param inputNum
	 * @return void
	 */
	public void isValidCoupleId(int InputNum) throws NoSuchElementException{
		repository.findById(InputNum);
	}
}
