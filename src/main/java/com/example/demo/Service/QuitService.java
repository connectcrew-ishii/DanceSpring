package com.example.demo.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Quit;
import com.example.demo.Entity.User;
import com.example.demo.Repository.QuitRepository;

@Service
public class QuitService {
	@Autowired
	QuitRepository repository;
	
	/**
	 * ユーザーの退会処理
	 * @param quit
	 * @return void
	 */
	public void save(Quit quit) {
		repository.save(quit);
	}
	
	/**
	 * 退会するユーザー情報の入力
	 * @param q
	 * @param u
	 * @return void
	 */
	public void setValue(Quit q,User u) {
		q.setNickname(u.getNickname());
		q.setName(u.getName());
		q.setKana(u.getKana());
		q.setPass(u.getPass());
		q.setMail(u.getMail());
		q.setSex(u.getSex());
		q.setYnum(u.getYnum());
		q.setAddress(u.getAddress());
		q.setPhone(u.getPhone());
		q.setMaga(u.getMaga());
		q.setEvent(u.getEvent());
		q.setEtc(u.getEtc());

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");

		q.setDate(sdf.format(date));
		q.setTime(sdf2.format(date));
	}
	
	/**
	 * 退会したユーザー一覧
	 * @return List<Quit>
	 */
	public List<Quit> quitList(){
		return repository.findAllByOrderByDate();
	}
}
