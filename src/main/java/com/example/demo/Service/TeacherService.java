package com.example.demo.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Couple;
import com.example.demo.Entity.Teacher;
import com.example.demo.Repository.TeacherRepository;

@Service
public class TeacherService {
	@Autowired
	TeacherRepository repository;

	public List<Teacher> teacherList(int coupleId){
		return repository.findByCoupleId(coupleId);
	}
	public List<Couple> teacherConfig(List<Couple> coupleList, int coupleId){
		List<Couple> list = new ArrayList<Couple>();
		for(Couple c:coupleList) {
			if(coupleId == c.getLeader().getCoupleId()) {
				list.add(c);
			}
		}
		return list;
	}

	public void updateTeacher(int coupleId, String name, String img, String intro) {
		repository.updateTeacher(coupleId, name, img, intro);
	}

	public void save(Teacher t) throws NoSuchElementException{
		repository.save(t);
	}

	public void deleteTeacher(int flag, int id) {
		Integer leader = 0, partner = 0;
		for(Teacher t:repository.findAll()) {
			if(t.getCoupleId() == id) {
				if(leader != 0) {
					partner = t.getId();
				}else {
					leader = t.getId();
				}
			}
		}
		repository.update(flag, leader);
		repository.update(flag, partner);
	}

	public void delete(Teacher teacher) {
		repository.delete(teacher);
	}

	public void update(Teacher teacher) {
		repository.save(teacher);
	}
}
