package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Contact;
import com.example.demo.Repository.ContactRepository;

@Service
public class ContactService {
	@Autowired
	ContactRepository repository;
	
	/**
	 * 問い合わせ履歴登録
	 * @param contact
	 * @return void
	 */
	public void create(Contact contact) {
		repository.save(contact);
	}
}
