package com.example.demo.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Reserve;
import com.example.demo.Entity.Schedule;
import com.example.demo.Entity.User;
import com.example.demo.Exception.DateParseException;
import com.example.demo.Exception.NullRecordException;
import com.example.demo.Exception.NumberFormedException;
import com.example.demo.Repository.UserRepository;

@Service
public class UserService {
@Autowired
UserRepository repository;

	public void update(User user) {
		repository.save(user);
	}

	public void updateMail(User user) throws SQLIntegrityConstraintViolationException{
		repository.save(user);
	}

	public void newUser(User user) throws SQLIntegrityConstraintViolationException, ConstraintViolationException {
		repository.save(user);
	}

	public void reRegister(User user) {
		for(User u:repository.findAll()) {
			if(u.getMail().equals(user.getMail())) {
				user.setId(u.getId());
				user.setFlag(0);
				break;
			}
		}
	}

	public List<User> userList(){
		List<User> userList = repository.findAll();
		return userList;
	}

	public Boolean isMailDeplicated(String mail) {
		for(User u:repository.findAll()) {
			if(u.getMail().equals(mail)) {
				if(u.getFlag() == 1) {
					return true;
				}
				return false;
			}
		}
		return true;
	}

	public boolean isLogin(String mail, String pass) {
		List<User>userList = repository.findAll();
		for(User u:userList) {
			if(u.getMail().equals(mail) && u.getPass().equals(pass)) {
				return u.getFlag() == 1 ? true : false;
				/*if(u.getFlag() == 1) {
					return false;
				}
				return true;
				*/
			}
		}
		return false;
	}

	public User setUser(String mail) {
		List<User> userList = repository.findByMail(mail);
		for(User u:userList) {
			if(mail.equals(u.getMail())) {
				return u;
			}
		}
		return null;
	}

	public User theUser(String mail){
		List<User> theUser = repository.findByMail(mail);
		return theUser.get(0);
	}

	public List<User> likeNickname(String text){
		return repository.findByNicknameLike(text);
	}
	public List<User> likeName(String text){
		return repository.findByNameLike(text);
	}
	public List<User> likeKana(String text){
		return repository.findByKanaLike(text);
	}
	public List<User> likePass(String text){
		return repository.findByPassLike(text);
	}
	public List<User> likeMail(String text){
		return repository.findByMailLike(text);
	}
	public List<User> likeYnum(String text){
		return repository.findByYnumLike(text);
	}
	public List<User> likeAddress(String text){
		return repository.findByAddressLike(text);
	}
	public List<User> likePhone(String text){
		return repository.findByPhoneLike(text);
	}
	public List<User> likeMaga(String text) throws NumberFormedException{
		try {
			return repository.findByMagaLike(Integer.parseInt(text));
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}

	}

	public List<User> likeFlag(String text) throws NumberFormedException{
		try {
			return repository.findByFlagLike(Integer.parseInt(text));
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}

	}

	public List<Reserve> getReserveList(User user) throws NullRecordException,DateParseException{
		List<Reserve> reserveList = new ArrayList<>();
		for(Schedule s:user.getScheduleList()) {
			Reserve r = new Reserve();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetime = s.getDate() + " " + s.getTime();
			try {
				Date d = sdf.parse(datetime);
				Date now = new Date();
				if(now.before(d)) {
					r.setId(s.getId());
					r.setDate(s.getDate());
					r.setTime(s.getTime());
					r.setCoupleId(s.getCouples().getId());
					r.setCouple(s.getCouples().getName());
					reserveList.add(r);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				throw new DateParseException(e);
			}
		}
		return reserveList;
	}

	public User findById(int id) {
		return repository.findById(id).get();
	}

	public void setMaga(User user, String maga) throws NumberFormedException{
		try {
			user.setMaga(Integer.parseInt(maga));
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}
	}

	public void setFlag(User user, String flag) throws NumberFormedException{
		try {
			user.setMaga(Integer.parseInt(flag));
		}catch(NumberFormatException e) {
			throw new NumberFormedException(e);
		}
	}

	public void deleteUser(int flag,int id) {
		repository.deleteUser(flag, id);
	}

	public void setAliveSchedule(User user) {
		List<Schedule> list = new ArrayList<Schedule>();
		for(Schedule s:user.getScheduleList()) {
			if(s.getFlag() == 0) {
				list.add(s);
			}
		}
		user.setScheduleList(list);
	}

	public List<User> mailList(){
		List<User> mailList = new ArrayList<>();
		for(User u:repository.findAll()) {
			if(u.getMaga() == 1 && u.getFlag() == 0) {
				mailList.add(u);
			}
		}
		return mailList;
	}
}
