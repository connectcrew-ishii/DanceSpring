package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Tsubu;
import com.example.demo.Repository.TsubuRepository;

@Service
public class TsubuService {
	@Autowired
	TsubuRepository repository;

	public List<Tsubu> tweetList(){
		return repository.findAllByOrderByIdDesc();
	}
	public void create(Tsubu t) {
		repository.save(t);
	}
}
