package com.example.demo.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.MyCalendar;
import com.example.demo.Entity.MyCalendarLogic;
import com.example.demo.Entity.Schedule;
import com.example.demo.Entity.User;
import com.example.demo.Repository.ScheduleRepository;

@Service
public class ScheduleService {
	@Autowired
	ScheduleRepository repository;
	
	/**
	 * 予約済みリストの最大Idを返す
	 * @return int
	 */
	public int findLastNumber() {
		int count = 0;
		List<Schedule> reserveList = repository.findAll();
		for(Schedule s:reserveList) {
			if(count < s.getId()) {
				count = s.getId();
			}
		}
		return count;
	}
	
	/**
	 * 月跨ぎ
	 * @param week
	 * @return int
	 */
	public int colspan(int week) {
		MyCalendarLogic cal = new MyCalendarLogic();
		List<MyCalendar> daytimeList = cal.dayTimeList(week);
		int count = 1;
		for(int i = 1; i < daytimeList.size(); i++) {
			if(daytimeList.get(0).getMonth() == daytimeList.get(i).getMonth()) {
				count += 1;
			}
		}
		return count;
	}
	
	/**
	 * 1週間の予約可否リスト
	 * @param week
	 * @param rList
	 * @param user
	 * @return List<ArrayList<MyCalendar>>
	 */
	public List<ArrayList<MyCalendar>> trueList(int week, List<Schedule> rList, User user) {
		String[] tList = new MyCalendar().getTimeList();
		List<MyCalendar> daytimeList = new MyCalendarLogic().dayTimeList(week);
		List<ArrayList<MyCalendar>> trueList = new ArrayList<ArrayList<MyCalendar>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(new Date());

		for(MyCalendar m:daytimeList) {
			ArrayList<MyCalendar> array = new ArrayList<MyCalendar>();
			if(m.getWeekday().equals("日")) {
				MyCalendar my = new MyCalendar();
				my.setNumber(4);
				array.add(my);
			}else {
				for(String s:tList){
					MyCalendar my = new MyCalendar();
					my.setYmday(m.getYmday());
					my.setTime(s);
					if(rList.isEmpty()) {
						my.setNumber(0);
					}else if(today.equals(my.getYmday())){
						my.setNumber(3);
					}else {
						for(Schedule sch:rList) {
							my.setNumber(0);
							if(m.getYmday().equals(sch.getDate()) && s.equals(sch.getTime())) {
								if(sch.getUserId() == user.getId()) {
									my.setNumber(2);
									break;
								}else {
									my.setNumber(1);
									break;
								}
							}
						}
					}
					array.add(my);
				}
			}
			trueList.add(array);
		}
		return trueList;
	}
	
	/**
	 * 予約情報の書き込み
	 * @param schedule
	 * @return void
	 */
	public void create(Schedule schedule) throws SQLIntegrityConstraintViolationException{
		repository.saveAndFlush(schedule);
	}
	
	/**
	 * 予約情報の書き込み?？
	 * @param s
	 * @return void
	 */
	public void save(Schedule s) {
		repository.save(s);
	}
	
	/**
	 * 予約DBから削除
	 * @param flag
	 * @param id
	 * @return void
	 */
	public void deleteSchedule(int flag, int id) {
		repository.update(flag, id);
	}
	
	/**
	 * 予約を削除？
	 * @param id
	 * @return void
	 */
	public void delete(Integer id) throws IllegalStateException{
		repository.deleteById(id);
	}
	
	/**
	 * 予約済みリスト
	 * @return List<Schedule>
	 */
	public List<Schedule> findAll(){
		return repository.findAll();
	}
	
	/**
	 * 予約情報の書き込み(時系列順)
	 * @param user
	 * @param schedule
	 * @return void
	 */
	public void addSchedule(User user, Schedule schedule) {
		String d = schedule.getDate() + " " + schedule.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int count = 0;
		try {
			Date date = sdf.parse(d);
			for(Schedule list:user.getScheduleList()) {
				Date listDate = sdf.parse(list.getDate() + " " + list.getTime());
				if(date.before(listDate)) {
					user.getScheduleList().add(count, schedule);
					break;
				}
				count++;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	/**
	 * 指定した[userId]の予約リスト
	 * @param id
	 * @return List<Schedule>
	 */
	public List<Schedule> findByUserId(int id){
		List<Schedule> list = new ArrayList<Schedule>();
		for(Schedule s:repository.findByUserIdOrderByDate(id)) {
			if(s.getFlag() == 0) {
				list.add(s);
			}
		}
		return list;
	}
}
