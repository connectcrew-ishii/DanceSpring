package com.example.demo.Entity;


import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@NotNull
public class Couple implements Serializable{
	private Teacher leader;
	private Teacher partner;
	private int coupleId;
	private String name;
}
