package com.example.demo.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "couples") //, schema = "d9spk1b9m1549h")
@Data
@NoArgsConstructor
@AllArgsConstructor
@NotNull
public class Couples implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String intro;
	@Column(name = "del_flag")
	private int flag;

	@OneToMany(orphanRemoval = true)
	//@Fetch(FetchMode.SUBSELECT)
	@JoinColumn(name = "couple_id", nullable = false, insertable = false, updatable = false)
	@OrderBy("date")
	private List<Schedule> scheduleList;

	@OneToMany(orphanRemoval = true)
	//@Fetch(FetchMode.SUBSELECT)
	@JoinColumn(name = "couple_id", nullable = false, insertable = false, updatable = false)
	private List<Teacher> teacherList;
}
