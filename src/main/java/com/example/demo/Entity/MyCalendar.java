package com.example.demo.Entity;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@NotNull
public class MyCalendar {
	private String[] timeList = {"12:00:00", "13:00:00", "14:00:00", "15:00:00", "16:00:00", "17:00:00", "18:00:00", "19:00:00", "20:00:00"};
	private int year;
	private int month;
	private int day;
	private String weekday;
	private String ymday;
	private int number;
	private String time;
}
