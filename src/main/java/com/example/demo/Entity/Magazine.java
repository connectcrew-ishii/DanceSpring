package com.example.demo.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "magazine") //, schema = "d9spk1b9m1549h")
@Data
@NoArgsConstructor
@AllArgsConstructor
@NotNull
public class Magazine implements Serializable{

	@Column(nullable = false)//←DBのカラムですよと宣言
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String date;
	private String time;
	private String title;
	private String text;
}
