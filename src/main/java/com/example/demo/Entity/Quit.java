package com.example.demo.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.example.demo.Validation.Phone;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "quit") //, schema = "d9spk1b9m1549h")
@Data
@NoArgsConstructor
@AllArgsConstructor
@NotNull
public class Quit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String date;
	private String time;
	private String nickname;
	private String name;
	private String kana;
	private int sex;
	private String pass;
	@Email
	private String mail;
	@Pattern(regexp = "[0-9]{7}$")
	private String ynum;
	private String address;
	@Phone
	private String phone;
	private int maga;
	@Column(name = "like_event")
	private int event;
	private String etc;
}
