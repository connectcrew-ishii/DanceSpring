package com.example.demo.Entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MyCalendarLogic {
	public List<MyCalendar> dayTimeList(int week) {
		List<MyCalendar> dayList = new ArrayList<MyCalendar>();
		SimpleDateFormat sdf1 = new SimpleDateFormat("E");
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd");

		for(int i = 0; i < 7; i++) {
			MyCalendar m = new MyCalendar();
			Calendar cl = Calendar.getInstance();
			cl.add(Calendar.DAY_OF_MONTH, i + week * 7);
			Date date = cl.getTime();
			String w = sdf1.format(date);
			m.setWeekday(w);
			String day = sdf3.format(date);
			m.setYmday(day);
			m.setYear(cl.get(Calendar.YEAR));
			m.setMonth(cl.get(Calendar.MONTH) + 1);
			m.setDay(cl.get(Calendar.DATE));

			dayList.add(m);
		}
		return dayList;
	}
	public int year() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		cal.get(Calendar.YEAR);
		Date date = cal.getTime();
		String ym = sdf.format(date);
		int y = Integer.parseInt(ym);
		return y;
	}
}
