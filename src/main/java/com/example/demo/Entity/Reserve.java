package com.example.demo.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reserve {
	//予約一覧の専用フィールド
	private int id;
	private int coupleId;
	private String date;
	private String time;
	private String couple;
}
