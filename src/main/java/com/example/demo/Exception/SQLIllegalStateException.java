package com.example.demo.Exception;

public class SQLIllegalStateException extends DanceException{
	private String status = "エラーコード：007";
	private String text = "情報の書き込み・取り出しに失敗しました。";

	public SQLIllegalStateException(Exception e) {
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}

}
