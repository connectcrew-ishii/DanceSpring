package com.example.demo.Exception;

public class ArgumentViolationException extends DanceException{
	private String status = "エラーコード：008";
	private String text = "メールアドレス・郵便番号・電話番号などで不正な文字が使われています。";

	public ArgumentViolationException(Exception e) {
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}

}
