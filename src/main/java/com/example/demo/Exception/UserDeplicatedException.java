package com.example.demo.Exception;

public class UserDeplicatedException extends DanceException{
	private String status = "エラーコード：001";
	private String text= "既に登録されたメールアドレスです。";

	public UserDeplicatedException(Exception e) {
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}
}
