package com.example.demo.Exception;

public class FormIllegalArgumentException extends DanceException{
	private String status = "エラーコード：002";
	private String text = "不正な値が入力されました。もう一度やり直してください。";

	public FormIllegalArgumentException(Exception e){
		super(e);
	}

	public FormIllegalArgumentException(){}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}
}
