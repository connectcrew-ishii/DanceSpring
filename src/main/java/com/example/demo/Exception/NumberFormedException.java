package com.example.demo.Exception;

public class NumberFormedException extends DanceException{
	private String status = "エラーコード：005";
	private String text = "入力値の変換に失敗しました。";

	public NumberFormedException(Exception e) {
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}
}
