package com.example.demo.Exception;

public class NullRecordException extends DanceException{
	private String status = "エラーコード：003";
	private String text = "データの取得に失敗しました。";

	public NullRecordException(Exception e){
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}
}
