package com.example.demo.Exception;

public class SessionTimeOutException extends DanceException{
	private String status = "エラーコード：009";
	private String text = "セッションがタイムアウトしました。最初からやり直してください。";

	public SessionTimeOutException(Exception e) {
		super(e);
	}
	public SessionTimeOutException() {}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}

}
