package com.example.demo.Exception;

public class NotFoundElementException extends DanceException{
	private String status = "エラーコード：006";
	private String text = "不正な値が入力されました。";

	public NotFoundElementException(Exception e) {
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}

}
