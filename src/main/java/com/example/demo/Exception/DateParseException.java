package com.example.demo.Exception;

public class DateParseException extends DanceException{
	private String status = "エラーコード004：";
	private String text = "日時が上手く取得出来ませんでした。";

	public DateParseException(Exception e) {
		super(e);
	}

	public String getStatus() {
		return status;
	}

	public String getText() {
		return text;
	}
}
